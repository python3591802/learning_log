from django.shortcuts import render
from .models import Topic

# Create your views here.
def index(request):
    """The home page for ll_app"""
    return render(request, 'll_app/index.html')

def topics(request):
    """The Home page for all topics"""
    topics = Topic.objects.order_by('date_added')
    context = {'topics':topics}
    return render(request, 'll_app/topics.html', context)

def topic(request,topic_id):
    """The home page for entry"""
    topic = Topic.objects.get(id=topic_id)
    entries = topic.entry_set.order_by('-date_added')
    context = {'topic': topic, 'entries': entries}
    return render(request, 'll_app/topic.html', context)