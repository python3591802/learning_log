"""Defines URL pattern for ll_app"""

from django.urls import path
from . import views

app_name = "ll_app"

urlpatterns = [
    
    #Home page
    path('', views.index, name='index'),

    #Topics page
    path('topics/', views.topics, name='topics'),

    #Entry page
    path('topics/<int:topic_id>', views.topic, name='topic')
]